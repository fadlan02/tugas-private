package com.nexsoft;

import java.util.Scanner;
import static java.lang.Math.sqrt;

class lingkaran{
    int r;
    float phi = 3.14F;


    public void luasDanKelilingLingkaran(){

    float luas = phi * r * r;
    float keliling = 2*phi*r;

        System.out.println("Luas lingkaran : " + luas);
        System.out.println("Keliling lingkaran : " + keliling);
    }
}

class segitiga {
    int alas;
    int tinggi;

    public void luasDanKelilingSegitiga(){

        int luas = (alas * tinggi)/2;
        double sisiMiring =sqrt((alas*alas)+(tinggi*tinggi));
        double keliling = alas + tinggi + sisiMiring;
        System.out.println("luas segitiga : " + luas);
        System.out.println("Keliling segitiga : " + keliling);
    }
}




public class SoalNo4 {
    public static void main(String[] args) {
    lingkaran jawabanLingkaran = new lingkaran();
        Scanner scan = new Scanner(System.in);
        System.out.println("Masukan nilai jari-jari : ");
        jawabanLingkaran.r = scan.nextInt();
    jawabanLingkaran.luasDanKelilingLingkaran();

        System.out.println("\n===========================\n");

    segitiga jawabanSegitiga = new segitiga();
        Scanner scan1 = new Scanner(System.in);
        System.out.println("Masukan nilai tinggi : ");
        jawabanSegitiga.tinggi = scan1.nextInt();
        System.out.println("Masukan Alas : ");
        jawabanSegitiga.alas = scan1.nextInt();


        jawabanSegitiga.luasDanKelilingSegitiga();
    }
}
