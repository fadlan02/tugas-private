package com.nexsoft;

import java.util.Scanner;

public class SoalNo1 {

    public void showData(){
        // scanner user input
        Scanner scan = new Scanner(System.in);
        System.out.println("Masukan angka : ");
        int userInput = scan.nextInt();

        Boolean bilanganPrima = true;
        // membuat looping
        for (int i = 2; i <= userInput/i ; i++){

//            temp = userInput % i;

            if (userInput % i == 0){
                bilanganPrima = false;
                break;
            }
        }

        if (bilanganPrima && ((userInput >= 0) && (userInput != 1))){
            System.out.println( userInput +" Adalah bilangan prima");
        }else{
            System.out.println(userInput + " Bukanlah bilangan prima");
        }


    }

    public static void main(String[] args) {
        SoalNo1 run = new SoalNo1();
        run.showData();
    }
}
